# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/sv/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Changes from public revision (TBD)


## [0.9.0] - 2023-05-29

### Added

### Fixed

### Changed

Removed duration and entry as attributes

### To be fixed/under review 


