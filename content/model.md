# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt ett evenemang och varje kolumn motsvarar en egenskap för det evenemanget. 20 attribut är definierade, där de första 5 är obligatoriska. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige. 


</div>


<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**name**](#id)|1|text|**Obligatoriskt** - Ange evenemangets namn.|
|[**description**](#description)|1|text|**Obligatoriskt** - Ange en kortare beskrivning av evenemanget. |
|[**type**](#type)|1|[concept](#type)|**Obligatoriskt** - Ange evenemangets typ. |
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Latitude anges per format enligt WGS84.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Longitude anges per format enligt WGS84.|
|[start_date](#start_date)|0..1|[dateTime](#datetime)| Ange startdatumet och starttiden för evenemanget enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html).|
|[end_date](#end_date)|0..1|[dateTime](#datetime)| Ange slutdatumet och sluttiden för evenemanget enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html).|
|[capacity](#capacity)|0..1|[heltal](#heltal)|Ange max antal besökare tillåtna på evenemanget.|
|[age_limit](#age_limit)|0..1|[heltal](#heltal)|Åldersgräns för evenemanget.|
|[price](#price)|0..1|[heltal](#heltal)|Ange pris för biljetter till evenemanget.|
|[free](#free)|0..1|[boolean](#boolean)|Ange om evenemanget är gratis.|
|[food](#food)|0..*|text|Ange vilken mat det serveras vid evenemanget.|
|[street](#street)|0..1|text|Ange gatuadress.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Ange postnummer.|
|[city](#city)|0..1|text|Ange postort.|
|[organizer](#organizer)|0..*|text|Ange evenemangets arrangör eller arrangörer.|
|[phone](#phone)|0..1|text|Ange telefonnumret till evenemangets arrangör med inledande landskod, exempelvis +46 |
|[email](#email)|0..1|text|E-postadress för vidare kontakt, anges med gemener och med @ som avdelare.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för mer information om evenemanget.|
|[image](#image)|0..*|[URL](#url)|En länk eller flera länkar till bilder på evenemanget.|



</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där evenemanget presenteras hos den lokala myndigheten eller arrangören.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

Exempel - ett evenemang som äger rum kl 17:00 den 22 februari 2023 

</br>
2023-02-22T17:00

</br>
</br>

20230222T1700


</div>

## Förtydligande av attribut

### name

Namnet på evenemanget.

### description

Beskrivning av evenemanget.

### type 

Typen av evenemang kan anges enligt nedan. Utåtgående länkar är till för att underlätta arbetet samt tydliggöra definitionen för varje typ av dessa evenemang.

[Business](https://schema.org/BusinessEvent) 
[Childrens](https://schema.org/ChildrensEvent)
[Comedy](https://schema.org/ComedyEvent)
[Dance](https://schema.org/DanceEvent)
[Delivery](https://schema.org/DeliveryEvent)
[Education](https://schema.org/EducationEvent)
[Exhibition](https://schema.org/ExhibitionEvent)
[Festival](https://schema.org/FestivalEvent)
[Food](https://schema.org/FoodEvent)
[Literary](https://schema.org/LiteraryEvent)
[Music](https://schema.org/MusicEvent)
[Publication](https://schema.org/PublicationEvent)
[Sale](https://schema.org/SaleEvent)
[Screening](https://schema.org/ScreeningEvent)
[Social](https://schema.org/SocialEvent)
[Sports](https://schema.org/SportsEvent)
[Theater](https://schema.org/TheaterEvent)   
[VisualArts](https://schema.org/VisualArtsEvent)

### latitude

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.


### start_date

Beskriver startdatum och även starttid för evenemanget. Här kan ni ange **YEAR-MONTH-DAY** enligt [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) för att ange eller **YEAR-MONTH-DAYThh:mm** om ni även önskar ange tiden evenemanget börjar. 

### end_date

Beskriver slutdatum för evenemanget och även sluttid för evenemanget. Här kan ni ange **YEAR-MONTH-DAY** enligt [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) för att ange eller **YEAR-MONTH-DAYThh:mm** om ni även önskar ange tiden evenemanget slutar. 

### capacity

Max antal besökare på evenemanget. Anges som heltal, exempelvis "50000". 

### age_limit

Åldersgräns för evenemanget. Anges som heltal, exempelvis om åldergränsen är 18 år: 18 

### price

Beskriver priset för biljetter till evenemanget. Anges med heltal. 

### free 

Beskriver om evenemanget är gratis eller inte. Här anges endast TRUE om det är känt att evenemanget är gratis och FALSE om evenemanget inte är gratis. Annars lämnar ni fältet tomt. 

### food

Beskriver vilken typ av mat som serveras vid evenemanget i text. 

### street

Evenemangets adress.

### postalcode

Postnummer till kommunen där evenemanget genomförs.

### city

Kommunen där evenemanget genomförs.

### organizer

Arrangörens namn. Exempelvis Vemdalsskalets Högfjällshotell.

### phone 

Telefonnummer till arrangören eller till platsen där evenemanget genomförs.

### email

Funktionsadress till den organisation som arrangerar evenemanget. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: info@organisation.se

### url 

Evenemangets eller organisatörens ingångssida. Anges med inledande schemat **https://** eller **http://** 

### image

En eller flera bildlänkar till evenemanget. Anges med inledande schemat **https://** eller **http://** 






 



