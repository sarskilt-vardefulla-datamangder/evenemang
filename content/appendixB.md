# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "name":  "Konferens",
    "description": "En kortare konferens på tisdag",
    "type": "Business",
    "latitude":  "58.399237",
    "longitude":  "15.417352",
    "start_date":  "2023:05:30:T08:30",
    "end_date":  "2023:05:30:T09:30",
    "capacity":  "50",
    "age_limit":  "18",
    "price":  "",
    "free":  "TRUE",
    "food":  "Vegetariskt",
    "street":  "",
    "postalcode":  "99999",
    "city":  "Göteborg",
    "organizer":  "Dataverkstad",
    "phone":  "",
    "email":  "",
    "URL":  "",
    "image":  "",      
}
```
