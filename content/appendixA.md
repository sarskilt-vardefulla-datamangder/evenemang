## Exempel i CSV, kommaseparerad

<div class="example csvtext">

name,description,type,latitude,longitude,start_date,end_date,capacity,age_limit,price,free,food,street,postalcode,city,organizer,phone,email,URL,image
<br>
Konferens,En kortare konferens på tisdag,Business,58.399237,15.417352,2023:05:30:T08:30,2023:05:30:T09:30,50,18,,TRUE,Vegetariskt,,99999,Göteborg,Dataverkstad,,,,
</div>

